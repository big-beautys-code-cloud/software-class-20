// 通过模块调用实现加减乘除，模块a实现加减乘除的业务逻辑并且暴露相关的方法，模块b调用a实现具体的业务
let a = 7;
let b = 7;
function puls(){
    console.log(a + b);
}
function reduce(){
    console.log(a - b);
}
function ride (){
    console.log(a * b);
}
function except(){
    console.log(a / b);
}

module.except = {
    puls : puls,
    reduce : reduce,
    ride : ride,
    except : except
}