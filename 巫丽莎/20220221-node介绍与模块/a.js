function add(A,B){
    return A + B;
}

function reduce(A,B){
    return A - B;
}
function multiply(A,B){
    return A * B;
}
function divide(A,B){
    return A / B;
}

//暴露
module.exports = {
    add:add,
    reduce:reduce,
    multiply:multiply,
    divide:divide
}