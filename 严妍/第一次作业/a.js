//通过模块调用实现加减乘除，模块a实现加减乘除的业务逻辑并且暴露相关的方法，模块b调用a实现具体的业务
let f = 2;
let g = 4;
function jia (){
    console.log(f+g);
}
function jian (){
    console.log(f-g);
}
function chen(){
    console.log(f*g);
}
function chu (){
    console.log(f/g);
}

module.exports = {
    jia : jia,
    jian : jian,
    chen : chen,
    chu : chu
}