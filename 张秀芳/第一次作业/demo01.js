//通过模块调用实现加减乘除，模块a实现加减乘除的业务逻辑并且暴露相关的方法，
//模块b调用a实现具体的业务
let a = 3;
let b = 2;
function add (){
    console.log(a+b);
}
function subtract  (){
    console.log(a-b);
}
function ride (){
    console.log(a*b);
}
function divide  (){
    console.log(a/b);
}

module.exports = {
    add : add,
    subtract : subtract,
    ride : ride,
    divide : divide
}